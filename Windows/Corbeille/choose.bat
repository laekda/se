<# : chooser.bat
:: launches a File... Open sort of file chooser and outputs choice(s) to the console
:: https://stackoverflow.com/a/15885133/1683264

@echo off
setlocal

for /f "delims=" %%I in ('powershell -noprofile "iex (${%~f0} | out-string)"') do (
	set full=%%~I
)

for /f "delims=" %%I in ('powershell -noprofile "iex (${%~dp0\chooseempty.txt} | out-string)"') do (
	set empty=%%~I
)

if "%empty%" NEQ "" (
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\CLSID\{645FF040-5081-101B-9F08-00AA002F954E}\DefaultIcon" /ve /t REG_EXPAND_SZ /d "%empty%,0" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\CLSID\{645FF040-5081-101B-9F08-00AA002F954E}\DefaultIcon" /v empty /t REG_EXPAND_SZ /d "%empty%,0" /f
)
if "%full%" NEQ "" (
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\CLSID\{645FF040-5081-101B-9F08-00AA002F954E}\DefaultIcon" /v full /t REG_EXPAND_SZ /d "%full%,0" /f
)

goto :EOF

: end Batch portion / begin PowerShell hybrid chimera #>

Add-Type -AssemblyName System.Windows.Forms
$f = new-object Windows.Forms.OpenFileDialog
$f.InitialDirectory = "C:\Users\$env:UserName\Pictures"
$f.Title = "Choisir une icone de corbeille pleine"
$f.Filter = "Icones (*.ico)|*.ico|Tous les fichiers (*.*)|*.*"
$f.ShowHelp = $true
$f.Multiselect = $false
[void]$f.ShowDialog()
if ($f.Multiselect) { $f.FileNames } else { $f.FileName }