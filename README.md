# Installation d'un nouveau système d'exploitation

Propose différent scripts et fichiers pour personnaliser rapidement son ordinateur lors de son installation.\
Certains fichiers ne sont là que pour automatiser le traitement d'une tâche longue.

Le dossier [Linux][linux] propose notamment des fichiers pour personnaliser son terminal.

Le dossier [Windows][windows] porpose des scripts pour automatiser certaines tâches fastifieuses.

Le script [bootcle][boot] est un script bash permettant de créer une clé bootable pour un système linux depuis un système linux. (Un dossier installation est prévu pour le futur)

[linux]: ./Linux/
[windows]: ./Windows/
[boot]: ./bootcle.sh0